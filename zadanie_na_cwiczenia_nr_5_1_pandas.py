import os
import zipfile
import pandas as pd
import glob

from zipfile import ZipFile

plik = input("Podaj lokalizację pliku (!!!bez cudzyslowia!!!):")
katalog = input("Podaj lokalizację folderu,gdzie plik ma być rozpakowany (!!!bez cudzyslowia!!!):")

with ZipFile(plik,'r') as file:
     file.extractall(katalog)

p=os.path.join(os.getcwd(),katalog)
lista_plikow = os.listdir(path=p)
files = glob.glob(p+"/*.csv")

glowna_tablica = []
# append datasets into the list
for i in range(len(lista_plikow)):
    df = pd.read_csv(files[i],encoding='utf-8')
    glowna_tablica.append(df)

data_top=list(glowna_tablica[0].columns)


# MINIMUM:
minimum_list = []
for i in range(len(glowna_tablica)):
    min = glowna_tablica[i]["temperatura"].idxmin()
    minimum_list.append(min)

minimum_table = []
for i in range(len(minimum_list)):
    ii = glowna_tablica[i]
    min_df = ii.iloc[minimum_list[i]]
    min_df = min_df.values.tolist()
    minimum_table.append(min_df)

tmin=pd.DataFrame(minimum_table, columns=data_top)
minmin = tmin["temperatura"].idxmin()
tmin = tmin.iloc[minmin,[1,2,3,4]]

temp_min = tmin["temperatura"]

temp_min_list = []
for i in range(len(glowna_tablica)):
    df = glowna_tablica[i]
    ids = df.index[df["temperatura"] == temp_min].tolist()
    temp_min_list.append(ids)

tempn_min_table = []
for i in range(len(temp_min_list)):
    o = glowna_tablica[i]
    for ii in range(len(temp_min_list[i])):
        g = o.iloc[temp_min_list[i][ii]]
        g = g.values.tolist()
        tempn_min_table.append(g)

minimum=pd.DataFrame(tempn_min_table, columns=data_top)
minimum=minimum[["stacja","data_pomiaru","godzina_pomiaru","temperatura"]]

# MAKSIMUM:
maximum_list = []
for i in range(len(glowna_tablica)):
    max = glowna_tablica[i]["temperatura"].idxmax()
    maximum_list.append(max)

maximum_table = []
for i in range(len(maximum_list)):
    ii = glowna_tablica[i]
    max_df = ii.iloc[maximum_list[i]]
    max_df = max_df.values.tolist()
    maximum_table.append(max_df)

tmax=pd.DataFrame(maximum_table, columns=data_top)
maxmax = tmax["temperatura"].idxmax()
tmax = tmax.iloc[maxmax,[1,2,3,4]]

temp_max = tmax["temperatura"]

temp_max_list = []
for i in range(len(glowna_tablica)):
    df = glowna_tablica[i]
    ids = df.index[df["temperatura"] == temp_max].tolist()
    temp_max_list.append(ids)

temp_max_table = []
for i in range(len(temp_max_list)):
    o = glowna_tablica[i]
    for ii in range(len(temp_max_list[i])):
        g = o.iloc[temp_max_list[i][ii]]
        g = g.values.tolist()
        temp_max_table.append(g)

maksimum=pd.DataFrame(temp_max_table, columns=data_top)
maksimum=maksimum[["stacja","data_pomiaru","godzina_pomiaru","temperatura"]]



# OPAD
opad_list = []
for i in range(len(glowna_tablica)):
    op = glowna_tablica[i]
    ids = op.index[op["suma_opadu"] > 0].tolist()
    opad_list.append(ids)

opad_table = []
for i in range(len(opad_list)):
    o = glowna_tablica[i]
    for ii in range(len(opad_list[i])):
        g = o.iloc[opad_list[i][ii]]
        g = g.values.tolist()
        opad_table.append(g)

opad=pd.DataFrame(opad_table, columns=data_top)
opady=opad[["stacja","godzina_pomiaru"]]


# WYNIKI:
print("\n" + "Najniższą temperaturę odnotowano:")
print(minimum.to_string()+"\n")
print(input("Wciśnij Enter aby kontynuować"))

print("\n" + "Najwyższą temperaturę odnotowano:")
print(maksimum.to_string()+"\n")
print(input("Wciśnij Enter aby kontynuować"))

print("\n" + "Stacje na ktorych zaobserwowano opady")
print(opady.to_string()+"\n")
print("\n"+"KONIEC PROGRAMU")


# NOTATKI I DODATKI
#PARE PLIKOW CSV JAKO OSOBNE TABLICE/MACIERZE:

# dataframes_list = []
 
# # append datasets into the list
# for i in range(len(lista_plikow)):
#     temp_df = pd.read_csv(lista_plikow[i],encoding='utf-8')
#     dataframes_list.append(temp_df)

# print(dataframes_list)
# print(type(dataframes_list))

# ////////////////////////////////////////////////////
#PARE PLIKOW CSV JAKO WEKTOR (TABLICA JEDNOWIERSZOWA):

# for i in range (1,len(lista_plikow)):             
#     with open(katalog + "\\" + lista_plikow[i-1],'r',encoding="utf8") as plikcsv:
#         pliki = csv.reader(plikcsv)
#         lista_naglowkow = next(pliki)
#         print(lista_naglowkow)
#         lista = []
#         for zmienna in pliki:
#             lista.append(zmienna)
# # print(pliki)
# print(lista)

# ////////////////////////////////////////////////////

# ZAMIEŃ PANDAS DATA FRAME NA LISTĘ, BLEEE
# tablica = final_content.values.tolist()
# print(tablica)

# ZAPISZ TABELĘ,która jest typu panda.data.frame DO CSV W WYBRANEJ LOKALIZACJI
# (ZMIENNA/wektor/tablica).to_csv("F:/D a w i d/DOKUMENTY/Studia/Geografia - geoinformatyka i kartografia/I rok/I semestr/Programowaie/29_11_2022/29_11_2022/file_name.csv", encoding='utf-8')
# ////////////////////////////////////////////////////

# POLECENIE:

# Pliki zawierają dane synoptyczne z polskich stacji pomiarowych na dzień 6 grudnia 2020 z godz.
# 14:00 UTC i 15:00 UTC. Opis danych znajduje się nagłówkach plików. Dane zostały pobrane z
# ogólnodostępnego serwisu https://danepubliczne.imgw.pl/
# Rozbuduj powyższy program tak, aby:
#  wypisywał, o której godzinie i na której stacji zanotowano minimalną temperaturę
# powietrza,
#  wypisywał, o której godzinie i na której stacji zanotowano maksymalną temperaturę
# powietrza,
#  wypisywał nazwy wszystkich stacji, na których odnotowano jakikolwiek opad
# atmosferyczny, oraz dopisywał koło nazw tych stacji godziny, kiedy ten opad wystąpił.
# Skrypty zsynchronizuj ze zdalnym repozytorium na GitLabie.
# ZADANIE 2
# Wejdź na stronę https://danepubliczne.imgw.pl/ i pobierz kilka plików CSV. Uruchom napisany
# wyżej skrypt na pobranych przez Ciebie danych.
