#Zadanie 1
tekst = "to jest testowe zdanie"
print(len(tekst))
print(tekst[12])

#Zadanie 2
lista = ["Jan","Ola"]
imie = input("Wprowadź swoje imię:")
lista.append(imie)
print(lista)

#Zadanie 3
listaImion =["Jan","Ola","Tomek",3,5,"kasia", "basia"]

print(listaImion[2])
print(len(listaImion))
print(listaImion[:3])
print(listaImion[3:])
print(listaImion[1:3])

Zadanie 4
Utwórz tablice numpy z podanej listy lista = [2,3,5,6,4,8,11,43,23] 
oraz wyświetl na ekranie wartość maksymalna i średnią. 
Utwórz kopię tablicy, która będzie zawierała 10 krotnie większę wartośći

import numpy as np

lista = [2,3,5,6,4,8,11,43,23] 
tablica = np.array(lista)
print(lista)
print(tablica)

max = tablica.max()
srednia = tablica.mean()
print(f"wartosc max to: {max}")
print(f"wartosc srednia to: {srednia}")

kopiaTablicy = tablica*10
print(kopiaTablicy)
kopialisty = lista*10
print(kopialisty)

