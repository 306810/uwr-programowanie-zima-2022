import time
import pandas as pd
import datetime

lokalizacja = input("Podaj lokalizacje zapsisu danych i oblcizne,bez cudzyslowia:")
# lokalizacja = "/home/dawid/Dokumenty/PROJEKT_ZAL" -> lokalizaczj a linuks/ubuntu 22.04
# lokalizacja = "D:\GIT-programowanie\Porjekt_zal" -> lokalizaccja win11
# lokalizacja lokalna,gdzie dane i wyniki zostana zapisane jako pliki csv
adres = "https://danepubliczne.imgw.pl/api/data/synop/format/csv"
tablica_poczatkowa = pd.read_csv(adres)
# dane ze strony aby potem uzytkownik mogl wybrac stacje wybierajac id

start = datetime.datetime.now().replace(microsecond=0)
# czas rozpoczecia pracy programu, w pliku koncowym

# time.sleep(15)
print("Dostępne stacje synoptyczne:")
print(tablica_poczatkowa[["id_stacji", "stacja"]].to_markdown())
id = int(input("Podaj id_stacji:"))
ids = tablica_poczatkowa.index[tablica_poczatkowa["id_stacji"] == id].to_list()
# nr/id wiersza jako zmienna ids

naglowki =[]
naglowki.append(tablica_poczatkowa)
data_top=list(naglowki[0].columns)

if len(ids) != 1:
    print("Stacja o podanym numerze id nie istnieje." + "\n" + "KONIEC PROGRAMU")
# wybiera 1 stacje, wiec, jesli wiecej lub mniej wartosci  to blad

else:
    spr = tablica_poczatkowa["stacja"]
    # kolumny z tablicy poczatkowej
    spr = spr.iloc[ids]
    # nr/id wiersza wybranej sracji
    print("Czy chodziło Ci o stację:"+ spr.to_string() + "?")
    print("Wciśnij ENTER aby potwierdzić bądź ESC aby zaprzeczyć")
    a = input()

    if a != "":
        print("Uruchum program ponownie")     
    else:
        time_s = tablica_poczatkowa.iloc[ids]
        time_s = time_s["godzina_pomiaru"]
        time_s = time_s.values.tolist()
        time_s = int(time_s[0])
        # time_s - godzina poczatkowa z tabeli danych pobranych z adresu i potem do ifa potrzebne
        
        tabela = pd.DataFrame(columns=data_top)
        wierszdf = tablica_poczatkowa.iloc[ids]
        tabela = tabela.append(wierszdf,ignore_index=True)
        # tabela- tabela z surowymi danymi, posluzy do obliczen. na starcie dodajemy 1 wiersz uzyskany od 
        # razu po odpaleniu programu

        print("PROGRAM POPROSI O KOLEJNO WPISANIE DATY I GODZINY, DO KTÓREJ MA ON DZIAŁAĆ")
        rok = input("Podaj rok w formacie YYYY:" + "\n")
        miesiac = input("Podaj miesiac w formacie liczbowym MM:" + "\n")
        dzien = input("Podaj dzien w formacie liczbowym DD:" + "\n")
        godzina = input("Podaj godzina w formacie liczbowym HH:MM" + "\n")
        # dane o dacie wprowadzone, ponizej zamiana zestreing to datatime

        data_dzienna = rok + "-" + miesiac + "-" + dzien
        data = data_dzienna + " " + godzina
        stop = datetime.datetime.strptime(data, '%Y-%m-%d %H:%M')
        # stop - dzienna data i godzina \, kiedy porgram zakonczy działanie

        def job():
            global time_n
            global df
            global teraz
            df = pd.read_csv(adres)
            tdf_ids = df.iloc[ids]
            tim = tdf_ids["godzina_pomiaru"]
            tim = tim.values.tolist()
            time_n = int(tim[0])
            teraz = datetime.datetime.now().replace(microsecond=0)
            print("WORK IN PROGRESS")
            print(teraz)
            return time_n
            # df - dane ze strony, tdf_ids - wiersz z wybrana stacja dzieki ids"id/nr wiersza"
            # tim - godzina pomiaru pobrana z tabeli
            # time_n - godzina pomiaru pobrana z tabeli w formacie int, time_n(ext), czyli nastepna godzina
            # z tabeli, rozna od time_s w tym przypadku o 1 godzine
      
        if stop < start:
            print("Data zakończenia programu nie może być wcześniejsza niż data obecna!Uruchom program ponownie")       
        else:
            teraz = datetime.datetime.now().replace(microsecond=0)
            job()
# zrob prace aby utworzyc zminne time_n
            while (teraz < stop) :
                # start_time = time.time() liczy start, a ponizej c stop i mierzy czas dzialania programu miedzy nimi
                if (time_s != time_n):
                    wierszdf = df.iloc[ids]
                    tabela = tabela.append(wierszdf,ignore_index=True)
                    time_s = time_n
                    next_07 = datetime.datetime.now().replace(minute=7,second=0,microsecond=0)
                    next_07 = next_07 + datetime.timedelta(hours=1)
                    # next_07, ze 07 minut po nastepnej pelnej godzinie
                    # c = float("%s" % (time.time() - start_time))
                    b = next_07 - teraz
                    b = b.total_seconds()
                    if next_07 >= stop:
                        delta = stop - datetime.datetime.now().replace(microsecond=0)
                        delta = delta.total_seconds()
                        time.sleep(delta)
                        job() 
                        if time_s != time_n:
                            wierszdf = df.iloc[ids]
                            tabela = tabela.append(wierszdf,ignore_index=True)
                        break
                    time.sleep(b)
                    job()                 
                else:
                    # c = float("%s" % (time.time() - start_time))
                    next_300s = datetime.datetime.now().replace(microsecond=0)
                    next_300s = next_300s + datetime.timedelta(minutes=5)
                    # next_300s, ze 300 s(5 minut odczekac)
                    d = next_300s - teraz
                    d = d.total_seconds()
                    if next_300s >= stop:
                        deltas = stop - datetime.datetime.now().replace(microsecond=0)
                        deltas = deltas.total_seconds()
                        time.sleep(deltas)
                        job() 
                        if time_s != time_n:
                            wierszdf = df.iloc[ids]
                            tabela = tabela.append(wierszdf,ignore_index=True)
                        break
                    time.sleep(d)
                    job()               
    
        tabela.to_csv(lokalizacja + "/surowe_dane.csv", encoding='utf-8') #zapisz panda data frame do pliku csv
        srednia_temp = tabela["temperatura"].mean()
        column_wynik = ["Data uruchomienia programu","Godzina uruchomienia programu","Data zakończenia programu","Godzina zakończenia programu","Średnia temperatura dla wybranego okresu"]
        wynik = pd.DataFrame(columns=column_wynik)
        poczatek = start.strftime('%Y-%m-%d %H:%M')
        koniec = stop.strftime('%Y-%m-%d %H:%M')
        data_start = start.strftime("%Y-%m-%d")
        data_koniec = stop.strftime("%Y-%m-%d")
        godz_start = start.strftime("%H:%M")
        godz_koniec = stop.strftime("%H:%M")
        wynik["Data uruchomienia programu"] = [data_start]
        wynik["Godzina uruchomienia programu"] = [godz_start]
        wynik["Data zakończenia programu"] = [data_koniec]
        wynik["Godzina zakończenia programu"] = [godz_koniec]
        wynik["Średnia temperatura dla wybranego okresu"] = [srednia_temp]
        wynik.to_csv(lokalizacja + "/wynik.csv", encoding='utf-8') 
        srednia_temp = str(srednia_temp)
        stacja = spr.to_string()
        stacja = stacja[6:]

print("\n" + "Program pracował od:" + "\n" + poczatek + "\n" + "do" + "\n"+ koniec + "\n")
time.sleep(2)
print("Dla tego okresu średnia temperatura dla stacji " + stacja + " wyniosła:" + "\n" + srednia_temp +"\n")
time.sleep(2)
print("Dane i obliczenia zostały zapisane w poniższej lokalizacji:" + "\n" + lokalizacja)
time.sleep(5)
print("\n"+"KONIEC PROGRAMU")


# ZROBIONO:
# v 1.0) czas sprawdzic,ale chyba git. Sprawdze jeszcze we Wrocku na kompie z WIN -> 02.01.2023 WIN OK, ŻE DZIAŁA
# v 1.1) podanie przez użytkownika na poczatku lokalizacji folderu, gdzie zapisac plik. LINUKS OK, chek WINDOWS -> 02.01.2023 WIN OK

# DO ZROBIENIA/POPRAWKI
# v 1.2) co jak w trakcie pracy zabraknie polaczenia z netem, to pewnie przerwie, a ja bym chcial aby 
# kontynuowac pomimo tego do czasu zakonczenia programu ( moze pech i chwilowy zanik czy cos)
# v 1.3) a moze niech caly czas zapisuje surowe dane do csv i nadpisuje aby razie wykraczenia dane zostaly zachowane 
# wraz z godzina crasha
# 4) nie wiem jak ze stabilnoscia

# a - alfa: działa na linuksie(ubuntu najnowsze)
# b- beta: działa na win11
# brak oznaczenia literowego -> działa na obu