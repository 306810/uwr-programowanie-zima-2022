import random as r
import os
#zadanie1
# a) petla for
for i in range(1,11):
    print(i)

# b) petla while
i = 1
while (i<=10):
    print(i)
    i=i+1

# ZADANIE 2
# 10 losowych liczb z zakresu od 1 do 30
# a) petla for
for i in range(1,11):
    print(r.randint(1,30))

# b) petla while
i = 1
while (i<=10):
    print(r.randint(1,30))
    i=i+1

# ZADANIE 3
zmienna = ""
while (True):
    zmienna = input("Podaj słowo python:")
    if (zmienna == "python"):
        break
print("Koniec programu")

# ZADANIE 4
# Skrypt do tworzenia listy plikow w folderze
lista_plikow = os.listdir("F:\D a w i d\DOKUMENTY\Studia\Geografia - geoinformatyka i kartografia\I rok\I semestr\SZKOLENIE_modelowanie_hydrologiczne\SZKOLENIE_modelowanie_hydrologiczne")
print(lista_plikow)