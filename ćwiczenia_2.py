Zadanie1
nazwisko = "Bartkowiak"
imie = "Dawid"

1 print("Czesc %s. Nazywam się %s." %(imie,nazwisko))
2 print("CZesc {}. Nazywam sie {}.".format(imie,nazwisko))
3 print(f"Czesc {imie}. Nazywam sie {nazwisko}")

Zadanie 2
Wyswietl liczbe pi z dokladnoscia 3 miejsc po przecinku
import math

pi = math.pi


print(pi)
 print("%.3f"%pi) ##"%.f"% - wyswietla liczbe zmiennoprzecinkowa, a "%.3f"%pi daje 3 miejsca po przecinku
print(("{:.3f}".format(pi)))
print(f"{pi:.3f}")

Zadanie 3
a = 3
b = 4.5
c = a+b

print(type(c))
print(isinstance(c,int))

Zadanie4
a = 3
b = 4.5
c = a+b

# print(type(c))
# print(isinstance(c,int))
if(isinstance(c,int)):
    print(f"Zmienna c jest typu int")
else:
    print(f"Zmienna c nie jest typu int")

Zadanie 5 - zółwik

import tkinter


import turtle
zolw = turtle.Turtle()
zolw.shape("turtle")

zolw.forward(100)
zolw.left(90)
zolw.forward(100)
zolw.left(90)
zolw.forward(100)
zolw.left(90)
zolw.forward(100)

turtle.exitonclick()

for i in range(10):
    zolw.circle(i*10)
turtle.exitonclick