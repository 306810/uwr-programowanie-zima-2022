import math
import tkinter
import turtle as zolw

# Zadanie 1
zmienna = 569728485348 * math.pi
# print(type(zmienna))

if(isinstance(zmienna,str) == True):
    print("Zmienna tekstowa")
elif ((isinstance(zmienna,int)) == True):
    print("Zmienna jest liczba calkowita")
elif ((zmienna.is_integer()) == True):
    print("Zmienna jest liczba rzeczywista o wartości całkowitej")
else:
    print("Zmienna jest liczba rzeczywista,niecałkowita")



# Zadanie 
# -> 2a)

predkosc = 1
bok = 200

zolw.speed(predkosc)
zolw.forward(bok)
zolw.left(90)
zolw.forward(bok)
zolw.left(90)
zolw.forward(bok)
zolw.left(90)
zolw.forward(bok)
zolw.forward(bok)
zolw.right(90)
zolw.forward(bok)
zolw.right(90)
zolw.forward(bok)
zolw.right(90)
zolw.forward(bok)

zolw.exitonclick()

# Zadanie 2
# -> 2b)

h_szer = 600 #h_szer-szerokosc domu
d_szer = 50 #d_szer - szerokosc drzwi
d_wys = 90 #d_wys - wysokosc drzwi
p_wys = 200 #p_wys - wysokosc kondygnacji parterowej
b_dach = math.sqrt (h_szer * h_szer * 0.5) #b_dach- dlugosc boku dachu (zal: trojkat rownoramienny,prostokatny)
predkosc2 = 1

zolw.setworldcoordinates(-400, 0, 400, 800)
zolw.speed(predkosc2)

zolw.forward(h_szer * 0.5)
zolw.left(90)
zolw.forward(p_wys)
zolw.left(90)
zolw.forward(h_szer)
zolw.right(135)
zolw.forward(b_dach)
zolw.right(90)
zolw.forward(b_dach)
zolw.right(135)
zolw.forward(h_szer)
zolw.left(90)
zolw.forward(p_wys)
zolw.left(90)
zolw.forward(h_szer * 0.5)
zolw.left(90)
zolw.forward(d_wys)
zolw.right(90)
zolw.forward(d_szer)
zolw.right(90)
zolw.forward(d_wys)


zolw.exitonclick()